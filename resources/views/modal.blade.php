<section id="popup-zoom" class="s-modal zoom-anim-dialog mfp-hide">

    <div class="container-fluid h-100">
        <div class="row h-100">

            <div class="close-modal mfp-close"> <img src="/img/icons/close-modal.svg" alt="Закрити"> </div>

            <div class=" col-md-6 col-xl-7 h-100">
                <div class="modal-container">

                    <div class="modal-logo">
                        <img src="/img/logo/gura-law-black.svg" alt="Gura Law - Професійне надання юридичних послуг">
                    </div>

                    <div class="modal-form">

                        <div class="title">Звяжіться з нами</div>
                        <div class="hr-line"><img src="/img/icons/line.svg" alt="Звяжіться з нами"></div>

                        <form action="#">
                            <div class="form-input">
                                <label>Ваше ім’я</label>
                                <input type="text">
                            </div>

                            <div class="form-input">
                                <label>Телефон або електронна пошта</label>
                                <input type="text">
                            </div>

                            <div class="form-select">
                                <label>Обрана послуга</label>
                                <select name="services" >
                                    <option value="1">Консультація з юристом</option>
                                    <option value="2">Пакет послуг для корпоративних клієнтів</option>
                                    <option value="3">Пакет послуг для ФОП</option>
                                    <option value="4">Пакет послуг сімейний</option>
                                </select>
                                <p class="modal-hint">Попередня вартість послуги: Безкоштовно</p>
                            </div>

                            <div class="form-btn-wrapper">
                                <button class="btn-accent btn-accent--img" type="submit"> <img src="/img/icons/send-white.svg" alt="Надіслати"> Надіслати</button>
                                <p class="modal-hint">Натиснувши кнопку Ви даєте згоду на обробку Ваших <a href="#">персональних данних</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="d-none d-md-block col-md-6 col-xl-5 h-100">

                <div class="maps">
                    <img class="tmp-map" src="/img/common/tmp-map.png" alt="Map">
                </div>

            </div>
        </div>
    </div>

</section>
