<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>

        <header id="header">
            <nav class="navigation navigation--white">
                <ul class="navigation-list">
                    <li class="navigation-item"><a class="navigation-link active" data-scroll="scroll-to" href="#header"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#register-company"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services-2"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services-3"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#contact"></a></li>
                </ul>
            </nav>

            <div class="nav-line">
                <div class="logo">
                    <a class="logo-link" href="/"><img class="logo-img" src="/img/logo/gura-law-white.svg" alt="Gura Law - Професійне надання юридичних послуг"></a>
                </div>

                <nav class="menu">
                    <div class="mnu-overlay"></div>

                    <div class="hamburger hamburger--collapse">
                        <div class="hamburger-box"> <div class="hamburger-inner"></div> </div>
                    </div>

                    <ul class="menu-list">
                        <li class="menu-item"><a class="menu-link" href="#header" data-scroll="scroll-to">Головна</a></li>
                        <li class="menu-item"><a class="menu-link" href="#register-company" data-scroll="scroll-to">Реєстрація компаній</a></li>
                        <li class="menu-item"><a class="menu-link" href="#services" data-scroll="scroll-to">Послуги</a></li>
                        <li class="menu-item"><a class="menu-link" href="#contact" data-scroll="scroll-to">Контакти</a></li>
                    </ul>
                </nav>
            </div>

            <div class="hero">
                <div class="container-fluid m-auto">
                    <div class="row align-items-center">

                        <div class="order-2 order-md-1 col-md-7 col-lg-6">
                            <div class="hero-text">
                                <h1 class="h1 mb-20">Професійне надання юридичних послуг</h1>
                                <p class="sub-title">від юристів з досвідом понад 15 років</p>
                                <a href="#popup-zoom" class="popup-zoom btn-accent">Запис на консультацію</a>
                            </div>
                        </div>

                        <div class="order-1 order-md-2 col-md-5 col-lg-6">
                            <div class="hero-img"> <img src="/img/common/statue.png" alt="Gura Law - Професійне надання юридичних послуг"> </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="header-contact">
                <ul class="social social-vertical">
                    <li class="social-item" ><a class="social-link" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                    <li class="social-item" ><a class="social-link" href="#"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="social-item" ><a class="social-link" href="#"><i class="fab fa-twitter"></i></a></li>
                </ul>

                <div class="mail-box">guralaw@mail.me</div>
            </div>
        </header>

        <section class="s-register-company" id="register-company">
            <nav class="navigation">
                <ul class="navigation-list">
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#header"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#register-company"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services-2"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services-3"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#contact"></a></li>
                </ul>
            </nav>

            <div class="section-container">

                <div class="section-container__item">
                    <div class="translate-box">
                        <div class="translate-box__container">
                            <img src="/img/common/company.svg" alt="Gura Law - Надаємо повний супровід при реєстрації нової компанії">
                        </div>
                    </div>
                </div>

                <div class="section-container__item">
                    <div class="description-container ml-lg-10p">
                        <h3 class="h3 mb-25">Надаємо повний супровід при реєстрації нової компанії</h3>
                        <a href="#popup-zoom" class="popup-zoom btn-accent">Зареєструвати компанію</a>
                    </div>
                </div>

            </div>
        </section>

        <section class="s-service" id="services">
            <nav class="navigation">
                <ul class="navigation-list">
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#header"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#register-company"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services-2"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services-3"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#contact"></a></li>
                </ul>
            </nav>

            <div class="section-container">

                <div class="section-container__item red-box">
                    <div class="img-box m-auto"> <img src="/img/common/family.svg" alt="Gura Law - Пакет послуг сімейний"> </div>
                </div>

                <div class="section-container__item">
                    <div class="description-container ml-sm-15p">
                        <h2 class="h2 mb-30">Пакет послуг сімейний</h2>
                        <a href="#popup-zoom" class="popup-zoom btn-accent">Замовити послугу</a>
                    </div>
                </div>

            </div>
        </section>

        <section class="s-service" id="services-2">
            <nav class="navigation navigation--white">
                <ul class="navigation-list">
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#header"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#register-company"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services-2"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services-3"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#contact"></a></li>
                </ul>
            </nav>

            <div class="section-container section-sm-inverse">

                <div class="section-container__item red-box">
                    <div class="img-box m-auto"> <img src="/img/common/fop.svg" alt="Gura Law - Пакет послуг для ФОП"> </div>
                </div>

                <div class="section-container__item">
                    <div class="description-container mr-sm-8p">
                        <h2 class="h2 mb-30">Пакет послуг для ФОП</h2>
                        <a href="#popup-zoom" class="popup-zoom btn-accent">Замовити послугу</a>
                    </div>
                </div>

            </div>
        </section>

        <section class="s-service" id="services-3">
            <nav class="navigation">
                <ul class="navigation-list">
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#header"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#register-company"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services-2"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services-3"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#contact"></a></li>
                </ul>
            </nav>

            <div class="section-container">

                <div class="section-container__item red-box">
                    <div class="img-box m-auto"> <img src="/img/common/clients.svg" alt="Gura Law - Пакет послуг для корпоративних клієнтів"> </div>
                </div>

                <div class="section-container__item">
                    <div class="description-container ml-sm-8p">
                        <h2 class="h2 mb-30">Пакет послуг для корпоративних клієнтів</h2>
                        <a href="#popup-zoom" class="popup-zoom btn-accent">Замовити послугу</a>
                    </div>
                </div>

            </div>
        </section>

        <section class="s-our-goal" id="contact">
            <nav class="navigation">
                <ul class="navigation-list">
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#header"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#register-company"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services-2"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#services-3"></a></li>
                    <li class="navigation-item"><a class="navigation-link" data-scroll="scroll-to" href="#contact"></a></li>
                </ul>
            </nav>

            <div class="our-goal-content">
                <h5 class="h5">Наша мета</h5>
                <div class="hr-line"><img src="/img/icons/line.svg" alt="Наша мета - робити складне, простим для Вас"></div>
                <h4 class="h4">Робити складне, простим для Вас</h4>
            </div>
        </section>

        <footer>
            <div class="contact-wrapper">

                <div class="contact-container">
                    <div class="sub-title">Зв'язок з нами</div>
                    <span class="contact-email">guralaw@mail.me</span>
                </div>

                <ul class="social social-horizontal">
                    <li class="social-item" ><a class="social-link" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                    <li class="social-item" ><a class="social-link" href="#"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="social-item" ><a class="social-link" href="#"><i class="fab fa-twitter"></i></a></li>
                </ul>

            </div>
        </footer>

        @include('modal')

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
