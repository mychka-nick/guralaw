$(document).ready(function() {
    // Toggle Burger Menu
    function toggleOpenMenu() {
        if ($(".hamburger").hasClass("is-active")) {
            $("body").removeClass("fixed-screen");
            $(".hamburger").removeClass("is-active");
            $(".menu").removeClass("is-open");
        } else {
            $(".menu").addClass("is-open");
            $(".hamburger").addClass("is-active");
            $("body").addClass("fixed-screen");
        }
    }

    function closeMenu() {
        if ($(".hamburger").hasClass("is-active")) {
            $("body").removeClass("fixed-screen");
            $(".hamburger").removeClass("is-active");
            $(".menu").removeClass("is-open");
        }
    }

    $(".hamburger").on("click", toggleOpenMenu);
    $(".mnu-overlay").on("click", toggleOpenMenu);

    // Close menu on click link
    $(".menu-link").on("click", function() {
        if (window.innerWidth < 768) toggleOpenMenu();
    });

    // Close menu on resize window
    $(window).resize(function () {
        if (window.innerWidth >= 768) closeMenu();
    })

    /*
     *  Initialization Magnific Popup plugin
     */
    $(".popup-zoom").magnificPopup({
        type: "inline",
        fixedContentPos: true,
        fixedBgPos: true,
        overflowY: "auto",
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: "my-mfp-zoom-in"
    });

    /*
     *  Initialization Scroll to id plugin
     */
    $("a[data-scroll='scroll-to']").mPageScroll2id({
        scrollSpeed: 900,
        scrollEasing: "easeInOutQuint",
        highlightClass: "active"
    });
});
